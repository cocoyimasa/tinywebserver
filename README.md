#Tiny Web Server

运行环境：MacOS

###How to install：

Xcode打开，直接运行即可。无任何其他依赖。
如果测试php支持，需要启动php-fpm（Listen端口：9000）。
即便后续添加其他依赖，也只依赖我自己的类库。

###关于
* 简单的Web Server。
* 通过Kqueue（mac下的epoll）支持高并发，ab测试单机7000无压力（mac下默认文件描述符有7000多，我没有修改默认值）。
* 支持静态和动态请求。
* 通过fastcgi支持php。