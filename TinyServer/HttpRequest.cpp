//
//  HttpRequest.cpp
//  TinyServer
//
//  Created by shenwudi on 8/22/16.
//  Copyright © 2016 shenwudi. All rights reserved.
//

#include "HttpRequest.hpp"

namespace swd
{
#define LOCALBUF 1024
    int is_conlength(char *str);
    int is_contype(char *str);
    
    HttpRequest::HttpRequest()
    {
        
    }
    HttpRequest::~HttpRequest()
    {
        
    }
    void HttpRequest::ClientError(int fd, const char *cause, const char *errnum,
                             const char *shortmsg, const char *longmsg)
    {
        char buf[MAXLINE], body[MAXBUF];
        
        /* Build the HTTP response body */
        sprintf(body, "<html><title>Tiny Error</title>");
        sprintf(body, "%s<body bgcolor=""ffffff"">\r\n", body);
        sprintf(body, "%s%s: %s\r\n", body, errnum, shortmsg);
        sprintf(body, "%s<p>%s: %s\r\n", body, longmsg, cause);
        sprintf(body, "%s<hr><em>The Tiny Web server</em>\r\n", body);
        
        /* 构建响应行，响应头部 */
        sprintf(buf, "HTTP/1.0 %s %s\r\n", errnum, shortmsg);
        Rio_writen(fd, buf, strlen(buf));
        sprintf(buf, "Content-type: text/html\r\n");
        Rio_writen(fd, buf, strlen(buf));
        sprintf(buf, "Content-length: %d\r\n\r\n", (int)strlen(body));//空行结束response headers
        Rio_writen(fd, buf, strlen(buf));
        Rio_writen(fd, body, strlen(body));
    }
    void HttpRequest::ParseHeader(int fd)
    {
        char buf[MAXLINE];
        // Robust IO 看CSAPP第10章
        
        /* Read request line and headers */
        rio_readinitb(&rio, fd);
        // 读取一行，在这里是request line
        rio_readlineb(&rio, buf, MAXLINE);
        // 解析出请求行中的HTTP 方法，uri，版本信息
        sscanf(buf, "%s %s %s", method, uri, version);
        if (strcasecmp(method, "GET") && strcasecmp(method, "POST")) {
            // 为了简单现在只是支持GET POST方法，非GET方法，就告知客户端我们无能为力
            ClientError(fd, method, "501", "Not Implemented",
                        "Tiny does not implement this method");
            return;
        }
    }
    void HttpRequest::ParseRequestBody()
    {
        char buf[MAXLINE];
        char *start, *end;
        
        memset(buf, 0, MAXLINE);
        int res = Rio_readlineb(&rio, buf, MAXLINE);
        check_if(res < 0, "rio_readlineb error");
        
        while (strcmp(buf, "\r\n") != 0) {
            
            start = index(buf, ':');
            // 每行数据包含\r\n字符，需要删除
            end = index(buf, '\r');
            
            if (start != 0 && end != 0) {
                *end = '\0';
                while ((*(start + 1)) == ' ') {
                    start++;
                }
                
                if (is_contype(buf)) {
                    strcpy(contype, start + 1);
                } else if (is_conlength(buf)) {
                    strcpy(conlength, start + 1);
                }
            }
            
            memset(buf, 0, MAXLINE);
            res = Rio_readlineb(&rio, buf, MAXLINE);
            check_if(res < 0, "rio_readlineb error");
            
        }
        return;
    }
    int is_conlength(char *str)
    {
        char *cur = str;
        char *cmp = "content-length";
        
        // 删除开始的空格
        while (*cur == ' ') {
            cur++;
        }
        
        for (; *cmp != '\0' && tolower(*cur) == *cmp; cur++,cmp++)
            ;
        
        if (*cmp == '\0') { // cmp字符串以0结束
            return 1;
        }
        
        return 0;
        
    }
    int is_contype(char *str)
    {
        char *cur = str;
        char *cmp = "content-type";
        
        // 删除开始的空格
        while (*cur == ' ') {
            cur++;
        }
        
        for (; *cmp != '\0' && tolower(*cur) == *cmp; cur++,cmp++)
            ;
        
        if (*cmp == '\0') { // cmp字符串以0结束
            return 1;
        }
        
        return 0;
        
    }
    
    int HttpRequest::ParseUri()
    {
        char *ptr, *query;;
        char uriStr[LOCALBUF];
        char *delim = ".php"; // 根据后缀名判断是静态页面还是动态页面
        char cwd[LOCALBUF];
        char *dir;
        
        strcpy(uriStr, uri); // 不破坏原始字符串
        
        if (!(query = strstr(uriStr, delim))) { // 静态页面
            strcpy(cgiargs, "");
            
            //  删除无用参数 /index.html?123435
            ptr = index(uriStr, '?');
            if (ptr) {
                *ptr = '\0';
            }
            
            strcpy(filename, ".");
            strcat(filename, uriStr);
            // 如果以‘/’结尾，自动添加index.html
            if (uriStr[strlen(uriStr) - 1] == '/') {
                strcat(filename, "index.html");
            }
            return 1;
        } else { // 动态页面
            // 提取查询参数
            //获取QUERY_STRING
            
            ptr = index(uriStr, '?');
            if (ptr) {
                strcpy(cgiargs, ptr + 1);
                *ptr = '\0';
            } else {
                // 类似index.php/class/method会提取class/method的参数
                if (*(query + sizeof(delim)) == '/') {
                    strcpy(cgiargs, query + sizeof(delim) + 1);
                    *(query + sizeof(delim)) = '\0';
                }
            }
            dir = getcwd(cwd, LOCALBUF); // 获取当前工作目录
            strcpy(filename, dir);       // 包含完整路径名
            strcat(filename, uriStr);
            strcpy(name, uriStr);         // 不包含完整路径名
            return 0;                                               
        }
    }
}