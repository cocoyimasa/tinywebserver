//
//  Server.hpp
//  TinyServer
//
//  Created by shenwudi on 8/21/16.
//  Copyright © 2016 shenwudi. All rights reserved.
//

#ifndef Server_hpp
#define Server_hpp
#include <iostream>
#include <cstdio>
#include "Base.hpp"

namespace swd
{
    class HttpRequest;
    
    class Server
    {
    public:
        enum{
            kReadEvent = 1,
            kWriteEvent = 2
        };
        int epollfd;
        HttpRequest* request;
    public:
        Server();
        ~Server();
        void start(int listenfd);
        int openListenfd(char* port);
        /*
         * get_filetype - derive file type from file name
         */
        void getFiletype(char *filename, char *filetype);
        /*
         * serve_dynamic - run a CGI program on behalf of the client
         */
        void serveDynamic(int fd, char *filename, char *cgiargs);
        /*
         * serve_static - copy a file back to the client
         */
        void serveStatic(int fd, char *filename, int filesize);
        
        
        /*
         * parse_uri - parse URI into filename and CGI args
         *             return 0 if dynamic content, 1 if static
         */
        int parseUri(char *uri, char *filename, char *cgiargs);
        
        /*
         * read_requesthdrs - read and parse HTTP request headers
         */
        void readRequesthdrs(rio_t *rp);
        /*
         * clienterror - returns an error message to the client
         */
        void clientError(int fd, const char *cause, const char *errnum,
                         const char *shortmsg, const char *longmsg);
        
        /*
         *  - handle one HTTP request/response transaction
         */
        void handleHttp(int fd);
        
        void setNonBlock(int fd);
        
        void updateEvents(int efd, int fd, int events, bool modify);
        
        void handleAccept(int efd, int fd);
        
        void handleRead(int efd, int connfd);
        
        void handleWrite(int efd, int fd);
        
        void loop_once(int efd, int lfd, int waitms);
        
        //FastCGI
        int openFastcgiClient();
        int sendFastcgi(int sock);
        int recvFastcgi(int fd, int sock);
    };
}
#endif /* Server_hpp */
