#ifndef _COMMON_H
#define _COMMON_H
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#include <cctype>
#include <cstdlib>
using namespace std;
namespace swd
{
#ifndef _UNICODE_SWD
	typedef std::string string_t;
	typedef char char_t;
	typedef std::ifstream ifstream_t;
	typedef std::ofstream ofstream_t;
	typedef std::istream istream_t;
	typedef std::ostream ostream_t;
	typedef std::stringstream stringstream_t;
#define T(X) X
#define strtol_t strtol
#define strtod_t strtod
#define strcmp_t strcmp
#define strncmp_t strncmp
#define strlen_t strlen
#define strncpy_t strncpy
#define strcpy_t strcpy
#else
	typedef std::wstring string_t;
	typedef wchar_t char_t;
	typedef std::wifstream ifstream_t;
	typedef std::wofstream ofstream_t;
	typedef std::wistream istream_t;
	typedef std::wostream ostream_t;
	typedef std::wstringstream stringstream_t;

#define T(X) L##X
#define strtol_t wcstol
#define strtod_t wcstod
#define strcmp_t wcscmp
#define strncmp_t wcsncmp
#define strlen_t wcslen
#define strncpy_t wcsncpy
#define strcpy_t wcscpy

#endif
}
#endif
