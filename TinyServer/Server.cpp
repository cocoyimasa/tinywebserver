//
//  Server.cpp
//  TinyServer
//
//  Created by shenwudi on 8/21/16.
//  Copyright © 2016 shenwudi. All rights reserved.
//

#include "Server.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
#include "Fastcgi.hpp"
#include "HttpRequest.hpp"

typedef struct sockaddr SA;
#define LISTENNQ 2000
#define exit_if(r, ...) if(r) {printf(__VA_ARGS__); printf("error no: %d error msg %s\n", errno, strerror(errno)); exit(1);}

namespace swd {
    ssize_t sendtoCli(int fd, int outlen, char *out,
                  int errlen, char *err, FCGI_EndRequestBody *endr);
    Server::Server()
    {
        epollfd = kqueue();    //epoll_create
        exit_if(epollfd < 0 , "epoll_create failed");
    }
    
    void Server::start(int listenfd)
    {
        setNonBlock(listenfd);
        updateEvents(epollfd, listenfd, kReadEvent, false);
        for (;;) { //实际应用应当注册信号处理函数，退出时清理资源
            loop_once(epollfd, listenfd, 10000);
        }
    }
    
    int Server::openListenfd(char* port)
    {
        return Open_listenfd(port);
    }
    typedef struct mime_type_s {
        const char *type;
        const char *value;
    } mime_type_t;
    mime_type_t zaver_mime[] =
    {
        {".html", "text/html"},
        {".xml", "text/xml"},
        {".xhtml", "application/xhtml+xml"},
        {".txt", "text/plain"},
        {".rtf", "application/rtf"},
        {".pdf", "application/pdf"},
        {".word", "application/msword"},
        {".png", "image/png"},
        {".gif", "image/gif"},
        {".jpg", "image/jpeg"},
        {".jpeg", "image/jpeg"},
        {".au", "audio/basic"},
        {".mpeg", "video/mpeg"},
        {".mpg", "video/mpeg"},
        {".avi", "video/x-msvideo"},
        {".gz", "application/x-gzip"},
        {".tar", "application/x-tar"},
        {".css", "text/css"},
        {NULL ,"text/plain"}
    };
    const char* get_file_type(const char *type)
    {
        if (type == NULL) {
            return "text/plain";
        }
        
        int i;
        for (i = 0; zaver_mime[i].type != NULL; ++i) {
            if (strcmp(type, zaver_mime[i].type) == 0)
                return zaver_mime[i].value;
        }
        return zaver_mime[i].value;
    }
    /*
     * get_filetype - derive file type from file name
     */
    void Server::getFiletype(char *filename, char *filetype)
    {
        for (int i = 0; zaver_mime[i].type != NULL; ++i) {
            if (strstr(filename, zaver_mime[i].type))
                strcpy(filetype, zaver_mime[i].value);
            else{
                strcpy(filetype, "text/plain");
            }
        }
    }
    /*
     * serve_dynamic - run a CGI program on behalf of the client
     */
    void Server::serveDynamic(int fd, char *filename, char *cgiargs)
    {
//        char buf[MAXLINE];
//        char *emptylist[] = { 0 };
//        
//        /* Return first part of HTTP response */
//        sprintf(buf, "HTTP/1.0 200 OK\r\n");
//        Rio_writen(fd, buf, strlen(buf));
//        sprintf(buf, "Server: Tiny Web Server\r\n");
//        Rio_writen(fd, buf, strlen(buf));
//        
//        if (Fork() == 0) { /* child */
//            /* Real server would set all CGI vars here */
//            setenv("QUERY_STRING", cgiargs, 1);
//            Dup2(fd, STDOUT_FILENO);         /* Redirect stdout to client */
//            Execve(filename, emptylist, environ); /* Run CGI program */
//        }
//        Wait(NULL); /* Parent waits for and reaps child */
        int sock;
        
        // 创建一个连接到fastcgi服务器的套接字
        sock = openFastcgiClient();
        
        // 发送http请求数据
        sendFastcgi(sock);
        
        // 接收处理结果
        recvFastcgi(request->rio.rio_fd, sock);
        
        close(sock); // 关闭与fastcgi服务器连接的套接字
    }
    /*
     * 接收fastcgi返回的数据
     */
    int Server::recvFastcgi(int fd, int sock) {
        int requestId;
        char *p;
        int n;
        
        requestId = sock;
        
        // 读取处理结果
        if (Fastcgi::recvRecord(sendtoCli, fd, sock, requestId) < 0) {
             fprintf(stderr,"recvRecord error" );
            return -1;
        }
        return 0;
    }
    /*
     * php处理结果发送给客户端
     */
    ssize_t sendtoCli(int fd, int outlen, char *out,
                    int errlen, char *err, FCGI_EndRequestBody *endr)
    {
        char *p;
        int n;
        
        char buf[MAXLINE];
        sprintf(buf, "HTTP/1.1 200 OK\r\n");
        sprintf(buf, "%sServer: Web Server\r\n", buf);
        sprintf(buf, "%sContent-Length: %d\r\n", buf, outlen + errlen);
        sprintf(buf, "%sContent-Type: %s\r\n\r\n", buf, "text/html");
        if (rio_writen(fd, buf, strlen(buf)) < 0) {
             fprintf(stderr,"write to client error" );
        }
        
        if (outlen > 0) {
            p = index(out, '\r');
            n = (int)(p - out);
            if (rio_writen(fd, p + 3, outlen - n - 3) < 0) {
                 fprintf(stderr,"rio_written error" );
                return -1;
            }
        }
        
        if (errlen > 0) {
            if (rio_writen(fd, err, errlen) < 0) {
                 fprintf(stderr,"rio_written error" );
                return -1;
            }
        }
        return 0;
    }
    int Server::sendFastcgi(int sock)
    {
        int requestId, i, l;
        char *buf;
        
        requestId = sock;
        
        // params参数名
        char *paname[] = {
            "SCRIPT_FILENAME",
            "SCRIPT_NAME",
            "REQUEST_METHOD",
            "REQUEST_URI",
            "QUERY_STRING",
            "CONTENT_TYPE",
            "CONTENT_LENGTH"
        };
        
        // 对应上面params参数名，具体参数值所在hhr_t结构体中的偏移
        int paoffset[] = {
            static_cast<int>((size_t) & (((HttpRequest *)0)->filename)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->name)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->method)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->uri)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->cgiargs)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->contype)),
            static_cast<int>((size_t) & (((HttpRequest *)0)->conlength))
        };
        
        // 发送开始请求记录
        if (Fastcgi::sendBeginRequestRecord(sock, requestId) < 0) {
            fprintf(stderr,"sendBeginRequestRecord error");
            return -1;
        }
        
        // 发送params参数
        l = sizeof(paoffset) / sizeof(paoffset[0]);
        for (i = 0; i < l; i++) {
            // params参数的值不为空才发送
            if (strlen((char *)(((long)this->request) + paoffset[i])) > 0) {
                if (Fastcgi::sendParamsRecord(sock,
                                              requestId,
                                              paname[i],
                                              strlen(paname[i]),(char *)(((long)this->request) + paoffset[i]),
                                              strlen((char *)(((long)request) + paoffset[i]))) < 0)
                {
                    fprintf(stderr,"sendParamsRecord error");
                    return -1;
                }
            }
        }
        
        // 发送空的params参数
        if (Fastcgi::sendEmptyParamsRecord(sock, requestId) < 0) {
            fprintf(stderr,"sendEmptyParamsRecord error");
            return -1;
        }
        
        // 继续读取请求体数据
        l = atoi(request->conlength);
        if (l > 0) { // 请求体大小大于0
            buf = (char *)malloc(l + 1);
            memset(buf, '\0', l);
            if (rio_readnb(&(request->rio), buf, l) < 0) {
                fprintf(stderr,"rio_readn error");
                free(buf);
                return -1;
            }
            
            // 发送stdin数据
            if (Fastcgi::sendStdinRecord( sock, requestId, buf, l) < 0) {
                fprintf(stderr,"sendStdinRecord error" );
                free(buf);
                return -1;
            }
            
            free(buf);
        }
        
        // 发送空的stdin数据
        if (Fastcgi::sendEmptyStdinRecord( sock, requestId) < 0) {
            fprintf(stderr,"sendEmptyStdinRecord error" );
            return -1;
        }
        
        return 0;
    }
    int Server::openFastcgiClient()
    {
        int sock;
        struct sockaddr_in serv_addr;
        
        // 创建套接字
        sock = socket(PF_INET, SOCK_STREAM, 0);
        if (-1 == sock) {
            fprintf(stderr,"socket error");
            return -1;
        }
        
        memset(&serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = inet_addr(FCGI_HOST);
        serv_addr.sin_port = htons(FCGI_PORT);
        
        // 连接服务器
        int r = connect(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
        if(-1 == r){
            fprintf(stderr,"connect error");
            return -1;
        }
        return sock;
    }
    /*
     * serve_static - copy a file back to the client
     */
    void Server::serveStatic(int fd, char *filename, int filesize)
    {
        int srcfd;
        char *srcp, filetype[MAXLINE], buf[MAXBUF];
        
        /* Send response headers to client */
        getFiletype(filename, filetype);
        sprintf(buf, "HTTP/1.0 200 OK\r\n");
        sprintf(buf, "%sServer: Tiny Web Server\r\n", buf);
        sprintf(buf, "%sContent-length: %d\r\n", buf, filesize);
        sprintf(buf, "%sContent-type: %s\r\n\r\n", buf, filetype);
        Rio_writen(fd, buf, strlen(buf));
        
        /* Send response body to client */
        srcfd = Open(filename, O_RDONLY, 0);
        srcp = (char*)Mmap(0, filesize, PROT_READ, MAP_PRIVATE, srcfd, 0);
        Close(srcfd);
        Rio_writen(fd, srcp, filesize);
        Munmap(srcp, filesize);
    }
    
    
    /*
     * parse_uri - parse URI into filename and CGI args
     *             return 0 if dynamic content, 1 if static
     */
    int Server::parseUri(char *uri, char *filename, char *cgiargs)
    {
        char *ptr;
        
        if (!strstr(uri, "cgi-bin")) {  /* Static content */
            strcpy(cgiargs, "");
            strcpy(filename, ".");
            strcat(filename, uri);
            if (uri[strlen(uri)-1] == '/')
                strcat(filename, "home.html");
            return 1;
        }
        else {  /* Dynamic content */
            ptr = index(uri, '?');
            if (ptr) {
                strcpy(cgiargs, ptr+1);
                *ptr = '\0';
            }
            else
                strcpy(cgiargs, "");
            
            strcpy(filename, ".");
            strcat(filename, uri);
            return 0;
        }
    }
    
    /*
     * read_requesthdrs - read and parse HTTP request headers
     */
    void Server::readRequesthdrs(rio_t *rp)
    {
        char buf[MAXLINE];
        
        Rio_readlineb(rp, buf, MAXLINE);
        while(strcmp(buf, "\r\n") != 0) {// 读取所有行，知道遇到空行，包括空行也会读到
            Rio_readlineb(rp, buf, MAXLINE);
            printf("%s", buf);
        }
        return;
    }
    
    /*
     * clienterror - returns an error message to the client
     */
    void Server::clientError(int fd, const char *cause, const char *errnum,
                             const char *shortmsg, const char *longmsg)
    {
        char buf[MAXLINE], body[MAXBUF];
        
        /* Build the HTTP response body */
        sprintf(body, "<html><title>Tiny Error</title>");
        sprintf(body, "%s<body bgcolor=""ffffff"">\r\n", body);
        sprintf(body, "%s%s: %s\r\n", body, errnum, shortmsg);
        sprintf(body, "%s<p>%s: %s\r\n", body, longmsg, cause);
        sprintf(body, "%s<hr><em>The Tiny Web server</em>\r\n", body);
        
        /* 构建响应行，响应头部 */
        sprintf(buf, "HTTP/1.0 %s %s\r\n", errnum, shortmsg);
        Rio_writen(fd, buf, strlen(buf));
        sprintf(buf, "Content-type: text/html\r\n");
        Rio_writen(fd, buf, strlen(buf));
        sprintf(buf, "Content-length: %d\r\n\r\n", (int)strlen(body));//空行结束response headers
        Rio_writen(fd, buf, strlen(buf));
        Rio_writen(fd, body, strlen(body));
    }
    
    /*
     * runServer - handle one HTTP request/response transaction
     */
    void Server::handleHttp(int fd)
    {
        int is_static;
        struct stat sbuf;
        //char buf[MAXLINE], method[MAXLINE], uri[MAXLINE], version[MAXLINE];
        //char filename[MAXLINE], cgiargs[MAXLINE];
        
        this->request = new HttpRequest;
        // Robust IO 看CSAPP第10章
        //rio_t rio;
        
//        /* Read request line and headers */
//        rio_readinitb(&rio, fd);
//        // 读取一行，在这里是request line
//        rio_readlineb(&rio, buf, MAXLINE);
//        // 解析出请求行中的HTTP 方法，uri，版本信息
//        sscanf(buf, "%s %s %s", method, uri, version);
//        if (strcasecmp(method, "GET")) {
//            // 为了简单现在只是支持GET方法，非GET方法，就告知客户端我们无能为力
//            clientError(fd, method, "501", "Not Implemented",
//                        "Tiny does not implement this method");
//            return;
//        }
        this->request = new HttpRequest;
        request->ParseHeader(fd);
        // 消耗请求头部
        //readRequesthdrs(&rio);
        request->ParseRequestBody();
        /* Parse URI from GET request */
        //is_static = parseUri(uri, filename, cgiargs);
        is_static = request->ParseUri();
        // 对应的资源不存在
        if (stat(request->filename, &sbuf) < 0) {
            clientError(fd, request->filename, "404", "Not found",
                        "Tiny couldn't find this file");
            return;
        }
        // 请求静态资源
        if (is_static) { /* Serve static content */
            if (!(S_ISREG(sbuf.st_mode)) || !(S_IRUSR & sbuf.st_mode)) {
                clientError(fd, request->filename, "403", "Forbidden",
                            "Tiny couldn't read the file");
                return;
            }
            serveStatic(fd, request->filename, sbuf.st_size);
        }
        else { /* Serve dynamic content */
//            if (!(S_ISREG(sbuf.st_mode)) || !(S_IXUSR & sbuf.st_mode)) {
//                clientError(fd, request->filename, "403", "Forbidden",
//                            "Tiny couldn't run the CGI program");
//                return;
//            }
            serveDynamic(fd, request->filename, request->cgiargs);
        }
    }
    
    void Server::setNonBlock(int fd) {
        int flags = fcntl(fd, F_GETFL, 0);
        exit_if(flags<0, "fcntl failed");
        int r = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
        exit_if(r<0, "fcntl failed");
    }
    
    void Server::updateEvents(int efd, int fd, int events, bool modify) {
        struct kevent ev[2];
        int n = 0;
        if (events & kReadEvent) {
            EV_SET(&ev[n++], fd, EVFILT_READ, EV_ADD|EV_ENABLE, 0, 0, (void*)(intptr_t)fd);
        } else if (modify){
            EV_SET(&ev[n++], fd, EVFILT_READ, EV_DELETE, 0, 0, (void*)(intptr_t)fd);
        }
        if (events & kWriteEvent) {
            EV_SET(&ev[n++], fd, EVFILT_WRITE, EV_ADD|EV_ENABLE, 0, 0, (void*)(intptr_t)fd);
        } else if (modify){
            EV_SET(&ev[n++], fd, EVFILT_WRITE, EV_DELETE, 0, 0, (void*)(intptr_t)fd);
        }
        printf("%s fd %d events read %d write %d\n",
               modify ? "mod" : "add", fd, events & kReadEvent, events & kWriteEvent);
        int r = kevent(efd, ev, n, NULL, 0, NULL);
        exit_if(r, "kevent failed ");
    }
    
    void Server::handleAccept(int efd, int fd) {
        struct sockaddr_in raddr;
        socklen_t rsz = sizeof(raddr);
        int cfd = accept(fd,(struct sockaddr *)&raddr,&rsz);
        exit_if(cfd<0, "accept failed");
        sockaddr_in peer, local;
        socklen_t alen = sizeof(peer);
        int r = getpeername(cfd, (sockaddr*)&peer, &alen);
        exit_if(r<0, "getpeername failed");
        printf("accept a connection from %s\n", inet_ntoa(raddr.sin_addr));
        setNonBlock(cfd);
        updateEvents(efd, cfd, kReadEvent|kWriteEvent, false);
    }
    
    void Server::handleRead(int efd, int connfd) {
        char buf[4096];
        //    int n = 0;
        //    while ((n=::read(fd, buf, sizeof buf)) > 0) {
        //        printf("read %d bytes\n", n);
        //        int r = ::write(fd, buf, n); //写出读取的数据
        //        //实际应用中，写出数据可能会返回EAGAIN，此时应当监听可写事件，当可写时再把数据写出
        //        exit_if(r<=0, "write error");
        //    }
        
        handleHttp(connfd);
        
        if (errno == EAGAIN || errno == EWOULDBLOCK)
            return;
        //exit_if(n<0, "read error"); //实际应用中，n<0应当检查各类错误，如EINTR
        printf("fd %d closed\n", connfd);
        Close(connfd);
    }
    
    void Server::handleWrite(int efd, int fd) {
        //实际应用应当实现可写时写出数据，无数据可写才关闭可写事件
        updateEvents(efd, fd, kReadEvent, true);
    }
    
    void Server::loop_once(int efd, int lfd, int waitms) {
        struct timespec timeout;
        timeout.tv_sec = waitms / 1000;
        timeout.tv_nsec = (waitms % 1000) * 1000 * 1000;
        const int kMaxEvents = 20;
        struct kevent activeEvs[kMaxEvents];
        int n = kevent(efd, NULL, 0, activeEvs, kMaxEvents, &timeout);
        printf("epoll_wait return %d\n", n);
        for (int i = 0; i < n; i ++) {
            int fd = (int)(intptr_t)activeEvs[i].udata;
            int events = activeEvs[i].filter;
            if (events == EVFILT_READ) {
                if (fd == lfd) {
                    handleAccept(efd, fd);
                } else {
                    handleRead(efd, fd);
                }
            } else if (events == EVFILT_WRITE) {
                handleWrite(efd, fd);
            } else {
                exit_if(1, "unknown event");
            }
        }
    }
    
    
    Server::~Server()
    {
        
    }
}

//    int optVal = 1;
//    struct sockaddr_in serverAddr;
//    int listenfd = socket(AF_INET, SOCK_STREAM, 0);
//    if(listenfd < 0 ){
//        return -1;
//    }
//    if(setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, (const void *)&optVal, sizeof(int))){
//        return -1;
//    }
//    bzero((char*)&serverAddr, sizeof(serverAddr));
//    serverAddr.sin_family = AF_INET;
//    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
//    serverAddr.sin_port = htons((unsigned short)port);
//
//    if(bind(listenfd,(SA*)&serverAddr,sizeof(serverAddr))<0)
//    {
//        return -1;
//    }
//
//    if(listen(listenfd, LISTENNQ) < 0)
//    {
//        return -1;
//    }
//
//    return listenfd;