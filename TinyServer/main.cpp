//
//  main.cpp
//  TinyServer
//
//  Created by shenwudi on 8/21/16.
//  Copyright © 2016 shenwudi. All rights reserved.
//

#include <iostream>
#include <cstdio>
#include "Server.hpp"
#include "Base.hpp"
#include <sys/socket.h>
#include <netinet/in.h>
using namespace swd;
using namespace std;
#define exit_if(r, ...) if(r) {printf(__VA_ARGS__); printf("error no: %d error msg %s\n", errno, strerror(errno)); exit(1);}
void testSplit()
{
    char src[] = "abc,cde,efg,ghijsssss ssssssss\0";
    
    std::vector<std::string> r = stringSplit(src, ",");
    
    for(int i=0;i<r.size();i++){
        cout<<r[i]<<endl;
    }
    string str = "abc,,cde,,efg,,ghijsssss ssssssss";
    
    std::vector<std::string> r1= stringSplit(str, ",");
    
    for(int i=0;i<r1.size();i++){
        cout<<r1[i]<<endl;
    }
}

int main(int argc, char **argv)
{
    int listenfd, connfd, port=8080, clientlen;
    struct sockaddr_in clientaddr;

    char* szBuffer = (char *)malloc(sizeof(int) + 1);
    
    memset(szBuffer, 0, sizeof(int) + 1);
    
    sprintf(szBuffer, "%d", port);           //整数转化为字符串
    
    //Start Server
    Server server;
    listenfd = Open_listenfd(szBuffer);      //打开监听套接字
    free(szBuffer);
    while (1) {
        clientlen = sizeof(clientaddr);
        // 连接请求
        //connfd = Accept(listenfd, (SA *)&clientaddr, (socklen_t*)&clientlen);
        printf("fd %d listening at %d\n", listenfd, port);
        server.start(listenfd);
    }
    
    
}