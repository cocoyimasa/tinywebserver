//
//  HttpRequest.hpp
//  TinyServer
//
//  Created by shenwudi on 8/22/16.
//  Copyright © 2016 shenwudi. All rights reserved.
//

#ifndef HttpRequest_hpp
#define HttpRequest_hpp

#include "Base.hpp"
namespace swd{
    class HttpRequest
    {
    public:
        HttpRequest();
        ~HttpRequest();
        void ParseHeader(int fd);
        void ParseRequestBody();
        int ParseUri();
        void ClientError(int fd, const char *cause, const char *errnum,const char *shortmsg, const char *longmsg);
    public:
        char uri[256];          // 请求地址
        char method[16];        // 请求方法
        char version[16];       // 协议版本
        char filename[256];     // 请求文件名(包含完整路径)
        char name[256];         // 请求文件名(不包含路径，只有文件名)
        char cgiargs[256];      // 查询参数
        char contype[256];      // 请求体类型
        char conlength[16];     // 请求体长度
        rio_t rio;
    };
}

#endif /* HttpRequest_hpp */
